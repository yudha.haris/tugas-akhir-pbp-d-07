part of '_widgets.dart';

class CustomAppBar extends StatefulWidget {
  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).bottomAppBarColor,
      elevation: 0,
      centerTitle: true,
      title: GestureDetector(
        onTap: () {
          Navigator.pushReplacement(context, CupertinoPageRoute(builder: (context) => HomePage()));
        },
        child: Container(
          padding: const EdgeInsets.only(top: 5, bottom: 2),
          child: Image.asset(
            'assets/image/logo_tc.png',
            height: MediaQuery.of(context).size.height / 15,
            fit: BoxFit.scaleDown,
          ),
        ),
      ),
    );
  }
}
