import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/homepage/homepage.dart';
import 'package:teman_covid/feature/obat/list_obat.dart';
import 'package:teman_covid/feature/profile/profile.dart';
import 'package:teman_covid/feature/register_login/login.dart';
import 'package:teman_covid/feature/register_login/register.dart';
import 'package:teman_covid/feature/sharepengalaman/stories.dart';
import 'package:teman_covid/feature/washyourlyrics/wash_input.dart';

part 'custom_appbar.dart';
part 'custom_listview.dart';
part 'main_drawer.dart';
part 'sidebar_menu_button.dart';
