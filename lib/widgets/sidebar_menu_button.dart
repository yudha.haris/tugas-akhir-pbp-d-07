part of '_widgets.dart';

class SideBarMenuButton extends StatelessWidget {
  final String name;
  final Icon icon;
  SideBarMenuButton(this.name, this.icon);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (name == 'Informasi Obat') {
          Navigator.push(
              context, CupertinoPageRoute(builder: (context) => ListObat()));
        } else if (name == 'Pengalaman') {
          Navigator.push(
              context, CupertinoPageRoute(builder: (context) => Stories()));
        } else if (name == 'WashYourLyrics') {
          Navigator.push(
              context, CupertinoPageRoute(builder: (context) => WashInput()));
        }
      },
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Row(
          children: [
            icon,
            Container(
              width: 10,
            ),
            Text(
              name,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
