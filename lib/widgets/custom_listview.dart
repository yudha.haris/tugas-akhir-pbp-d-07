part of '_widgets.dart';

class CustomListView extends StatelessWidget {
  List<dynamic> items;
  CustomListView({required this.items});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: items.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return InkWell(
            child: Card(
              margin: EdgeInsets.only(
                  left: CustomSize.maxWidth(context) / 20,
                  right: CustomSize.maxWidth(context) / 20,
                  top: 10),
              elevation: 3,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              color: CupertinoColors.extraLightBackgroundGray,
              child: Column(
                children: [
                  Container(
                      padding: EdgeInsets.only(bottom: 10, left: 20, right: 20, top: 10),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        items[index].getSender(),
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      )),
                  Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: Text(items[index].getBody())),
                ],
              ),
            ),
          );
        });
  }
}
