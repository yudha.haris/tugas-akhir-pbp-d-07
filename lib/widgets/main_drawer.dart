part of '_widgets.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.maxWidth(context) / 1.5,
      color: Colors.indigo[50],
      height: CustomSize.maxHeight(context),
      child: SafeArea(
        child: Consumer<LoginState>(builder: (context, loginState, _) {
          return Column(
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => Profile(
                                username: loginState.username,
                              )));
                },
                child: Container(
                  width: CustomSize.maxWidth(context) / 1.5,
                  child: loginState.isLogin
                      ? Icon(
                          Icons.account_circle_sharp,
                          size: CustomSize.maxWidth(context) / 2,
                          color: Colors.blueGrey,
                        )
                      : Image.asset(
                          'assets/image/logo_tc.png',
                          height: MediaQuery.of(context).size.height / 5,
                          fit: BoxFit.scaleDown,
                        ),
                ),
              ),
              if (loginState.isLogin)
                Container(
                  width: CustomSize.maxWidth(context) / 1.5,
                  padding: EdgeInsets.only(top: 5, bottom: 20),
                  alignment: Alignment.center,
                  child: Text(
                    'Hi ' + loginState.username,
                    style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 25),
                  ),
                ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                      color: Theme.of(context).bottomAppBarColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10), topRight: Radius.circular(10))),
                  child: Column(
                    children: [
                      if (loginState.isLogin)
                        Column(
                          children: [
                            SideBarMenuButton(
                                'Informasi Obat',
                                const Icon(
                                  Icons.medical_services,
                                  color: Colors.white,
                                )),
                            SideBarMenuButton(
                                'Pengalaman',
                                const Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                )),
                            SideBarMenuButton(
                                'WashYourLyrics',
                                const Icon(
                                  Icons.music_note,
                                  color: Colors.white,
                                )),
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                              child: ElevatedButton(
                                onPressed: () {
                                  loginState.setLogin(false);
                                  loginState.setUsername('Friend');
                                  Navigator.pushReplacement(context,
                                      CupertinoPageRoute(builder: (context) => HomePage()));
                                },
                                child: Container(
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    child: Text('Logout')),
                                style: ElevatedButton.styleFrom(
                                    primary: Theme.of(context).cardColor,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10))),
                              ),
                            ),
                          ],
                        ),
                      if (!loginState.isLogin)
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      CupertinoPageRoute(builder: (context) => Register()));
                                },
                                child: Container(
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    child: Text('Create an Account!')),
                                style: ElevatedButton.styleFrom(
                                    primary: Theme.of(context).cardColor,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10))),
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Do you have account?',
                                ),
                                InkWell(
                                    onTap: () {
                                      Navigator.push(context,
                                          CupertinoPageRoute(builder: (context) => Login()));
                                    },
                                    child: Text(' Log in', style: TextStyle(color: Colors.white)))
                              ],
                            )
                          ],
                        ),
                    ],
                  ),
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
