import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorState with ChangeNotifier {
  Color mainColor = CupertinoColors.white;
  Color fontColor = Colors.black;
  Color textFieldColor = Colors.grey.shade200;
  bool darkMode = false;

  void turnOnDarkMode() {
    darkMode = true;
    mainColor = CupertinoColors.black;
    fontColor = CupertinoColors.white;
    textFieldColor = CupertinoColors.systemFill;
    notifyListeners();
  }

  void turnOffDarkMode() {
    darkMode = false;
    mainColor = CupertinoColors.white;
    fontColor = Colors.black;
    textFieldColor = Colors.grey.shade200;
    notifyListeners();
  }
}
