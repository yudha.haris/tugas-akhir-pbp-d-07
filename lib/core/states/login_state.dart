import 'package:flutter/cupertino.dart';

class LoginState with ChangeNotifier {
  String _username = '';
  bool _isLogin = false;

  get username => _username;
  get isLogin => _isLogin;

  void setUsername(String value) {
    _username = value;
    notifyListeners();
  }

  void setLogin(bool value) {
    _isLogin = value;
    notifyListeners();
  }
}
