import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/homepage/homepage.dart';

import 'core/states/color_state.dart';

class AppWrapper extends StatefulWidget {
  @override
  State<AppWrapper> createState() => _AppWrapperState();
}

class _AppWrapperState extends State<AppWrapper> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ColorState>(create: (context) => ColorState()),
        ChangeNotifierProvider<LoginState>(create: (context) => LoginState()),
      ],
      child: Consumer<ColorState>(builder: (context, colorState, _) {
        return MaterialApp(
          title: "temancovid",
          theme: ThemeData(
              hoverColor: Color(0xFFD9E89E),
              cardColor: Color(0xFF4C6245),
              bottomAppBarColor: const Color(0xFF8DB580)),
          home: HomePage(),
        );
      }),
    );
  }
}
