import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/homepage/formfeedback.dart';
import 'package:teman_covid/feature/obat/list_obat.dart';
import 'package:teman_covid/feature/sharepengalaman/stories.dart';
import 'package:teman_covid/feature/washyourlyrics/wash_input.dart';

import '../../widgets/_widgets.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: Container(
            padding: EdgeInsets.only(top: 40, left: 20, right: 20),
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 150,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        height: 150,
                        width: 370,
                        margin: const EdgeInsets.only(right: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color(0xFF7E8987),
                        ),
                        child: Image.asset(
                          "assets/image/illust.png",
                          height: 150,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 370,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.grey.shade200,
                        ),
                        child: const Text(
                          "\nTeman untuk kalian\ndi tengah kondisi pandemi :D",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        padding: EdgeInsets.all(20),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        child: Material(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context, CupertinoPageRoute(builder: (context) => ListObat()));
                        },
                        splashColor: const Color(0xFF4C6245),
                        borderRadius: BorderRadius.circular(20),
                        child: Ink(
                          child: const Icon(Icons.medical_services, color: Colors.white, size: 60),
                          width: 90,
                          height: 90,
                          decoration: BoxDecoration(
                              color: const Color(0xFF8DB580),
                              borderRadius: BorderRadius.circular(20)),
                        ),
                      ),
                    )),
                    Container(
                        child: Material(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context, CupertinoPageRoute(builder: (context) => Stories()));
                        },
                        splashColor: const Color(0xFF4C6245),
                        borderRadius: BorderRadius.circular(20),
                        child: Ink(
                          child: const Icon(Icons.edit, color: Colors.white, size: 60),
                          width: 90,
                          height: 90,
                          decoration: BoxDecoration(
                              color: const Color(0xFF8DB580),
                              borderRadius: BorderRadius.circular(20)),
                        ),
                      ),
                    ))
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        child: const Text("Informasi Obat",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))),
                    Container(
                        child: const Text("Pengalaman",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Consumer<LoginState>(builder: (context, loginState, _) {
                      return Container(
                          child: Material(
                        child: InkWell(
                          onTap: () {
                            if (loginState.isLogin) {
                              Navigator.push(
                                  context, CupertinoPageRoute(builder: (context) => WashInput()));
                            } else {
                              showDialog<void>(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                      title: Text('Whoops :(',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Theme.of(context).cardColor)),
                                      content: SingleChildScrollView(
                                        child: ListBody(
                                          children: <Widget>[
                                            Text('Anda harus login terlebih dahulu',
                                                style: TextStyle(
                                                    color: Theme.of(context).bottomAppBarColor)),
                                          ],
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          child: const Text('OK'),
                                          onPressed: () {
                                            // Go back to page before pop-up
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                      elevation: 24.0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(20.0)));
                                },
                              );
                            }
                          },
                          splashColor: const Color(0xFF4C6245),
                          borderRadius: BorderRadius.circular(20),
                          child: Ink(
                            child: const Icon(Icons.music_note, color: Colors.white, size: 60),
                            width: 90,
                            height: 90,
                            decoration: BoxDecoration(
                                color: const Color(0xFF8DB580),
                                borderRadius: BorderRadius.circular(20)),
                          ),
                        ),
                      ));
                    }),
                    Container(
                        child: Material(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context, CupertinoPageRoute(builder: (context) => FormFeedback()));
                        },
                        splashColor: const Color(0xFF4C6245),
                        borderRadius: BorderRadius.circular(20),
                        child: Ink(
                          child: const Icon(Icons.feedback, color: Colors.white, size: 60),
                          width: 90,
                          height: 90,
                          decoration: BoxDecoration(
                              color: const Color(0xFF8DB580),
                              borderRadius: BorderRadius.circular(20)),
                        ),
                      ),
                    ))
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        child: const Text("WashYourLyrics",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))),
                    Container(
                      child: const Text("Feedback",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                  ],
                ),
              ],
            )));
  }
}
