import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../widgets/_widgets.dart';

class FormFeedback extends StatefulWidget {
  @override
  _FormFeedbackState createState() => _FormFeedbackState();
}

class _FormFeedbackState extends State<FormFeedback> {
  final TextEditingController _feedbackController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(
        margin: const EdgeInsets.only(left: 30, top: 30, right: 50, bottom: 7),
        child: SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 20),
                child: const Text(
                  "Form Feedback",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 50, left: 20, bottom: 20),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: const Color(0xFF8DB580), width: 2)),
                child: TextFormField(
                  style: const TextStyle(fontSize: 17),
                  maxLines: null,
                  controller: _feedbackController,
                  textInputAction: TextInputAction.done,
                  decoration: const InputDecoration(
                    hintText: "Ketik di sini buat ngasih feedback :D",
                    contentPadding: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 100),
                    border: InputBorder.none,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 50, right: 30, bottom: 30),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: const Color(0xFF8DB580)),
                  child: const Text("Submit Feedback"),
                  onPressed: () async {
                    if (_feedbackController.text != '') {
                      final http.Response response = await http
                          .post(Uri.parse("https://temancovid.herokuapp.com/postfeedback/"), body: {
                        "feedback": _feedbackController.text,
                      });
                      setState(() {
                        _feedbackController.text = '';
                      });

                      showDialog<void>(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text("Terima kasih!"),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: const <Widget>[
                                    Text("Feedback kamu sudah sudah kami terima :D"),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text("OK"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ],
                            );
                          }
                      );
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
