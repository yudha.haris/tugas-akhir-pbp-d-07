part of '../../feature/commentpengalaman/_screens.dart';

class CommentPengalaman extends StatefulWidget {
  final String postTitle;
  final String postSender;
  final String postBody;
  final String slug;
  const CommentPengalaman(
      {Key? key,
      required this.postTitle,
      required this.postSender,
      required this.postBody,
      required this.slug})
      : super(key: key);

  @override
  _CommentPengalamanState createState() => _CommentPengalamanState();
}

class _CommentPengalamanState extends State<CommentPengalaman> {
  TextEditingController _noteController = TextEditingController();
  TextEditingController _titleController = TextEditingController();
  bool _showComments = true;
  bool showTopModals = false;
  bool _isLogin = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Consumer<LoginState>(builder: (context, loginState, _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(top: 20, bottom: 30),
                child: const FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    'Share Your Stories!',
                    style: TextStyle(fontWeight: FontWeight.w900, fontSize: 30),
                  ),
                ),
              ),
              StoryBody(
                  title: widget.postTitle, sender: widget.postSender, content: widget.postBody),
              Container(
                height: 30,
              ),
              if (_showComments)
                FutureBuilder<List<dynamic>>(
                    future: ServicesCommentPengalaman.fetchKomen(widget.slug),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: snapshot.data!.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              return InkWell(
                                child: Card(
                                  margin: EdgeInsets.only(
                                      left: CustomSize.maxWidth(context) / 20,
                                      right: CustomSize.maxWidth(context) / 20,
                                      top: 10),
                                  elevation: 3,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  color: CupertinoColors.extraLightBackgroundGray,
                                  child: Column(
                                    children: [
                                      Container(
                                          padding: const EdgeInsets.only(
                                              bottom: 10, left: 20, right: 20, top: 10),
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            snapshot.data![index]['fields']['oleh'].toString(),
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold, fontSize: 20),
                                          )),
                                      Container(
                                          alignment: Alignment.centerLeft,
                                          padding: const EdgeInsets.only(
                                              left: 20, right: 20, bottom: 10),
                                          child: Text(
                                              snapshot.data![index]['fields']['body'].toString())),
                                    ],
                                  ),
                                ),
                              );
                            });
                      }
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    }),
              if (loginState.isLogin)
                Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 0, top: 20, left: 20, right: 20),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _showComments = !_showComments;
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            (_showComments) ? 'Hide Comments' : 'Show Comments',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            side: BorderSide(
                              width: 1.0,
                              color: Theme.of(context).bottomAppBarColor,
                            ),
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    TextFieldCommentPengalaman(controller: _titleController, isTitle: true),
                    TextFieldCommentPengalaman(controller: _noteController, isTitle: false),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_noteController.text.isNotEmpty && _titleController.text.isNotEmpty) {
                            await ServicesCommentPengalaman.postKomen(
                                _titleController.text, _noteController.text, widget.slug);
                            await ServicesCommentPengalaman.fetchKomen(widget.slug);
                            setState(() {
                              _titleController.text = "";
                              _noteController.text = "";
                            });
                          }
                        },
                        child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            child: const Text('Add Comment')),
                        style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).cardColor,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    Container(
                      color: Theme.of(context).bottomAppBarColor,
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(top: 15, right: 10, left: 10, bottom: 20),
                      width: CustomSize.maxWidth(context),
                      child: const Text(
                        'Copyright © 2021 PBP D07 University of Indonesia',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              if (!loginState.isLogin)
                Container(
                  padding: const EdgeInsets.only(top: 20, bottom: 30),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      'Login to add comment',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: Theme.of(context).cardColor),
                    ),
                  ),
                ),
            ],
          );
        }),
      ),
    );
  }
}
