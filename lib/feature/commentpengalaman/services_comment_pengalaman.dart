import 'dart:convert';

import 'package:http/http.dart' as http;

class ServicesCommentPengalaman {
  static Future<List<dynamic>> fetchKomen(String slug) async {
    final response =
        await http.get(Uri.parse('https://temancovid.herokuapp.com/' + slug + '/json'));

    if (response.statusCode == 200) {
      List<dynamic> komens = jsonDecode(response.body);
      return komens;
    } else {
      return [];
    }
  }

  static Future<void> postKomen(String oleh, String komen, String slug) async {
    final response = await http.post(
        Uri.parse('https://temancovid.herokuapp.com/' + slug + '/post'),
        body: {'body': komen, 'oleh': oleh});
  }
}
