part of '_screens.dart';

class TextFieldCommentPengalaman extends StatefulWidget {
  TextEditingController controller;
  final bool isTitle;
  TextFieldCommentPengalaman({Key? key, required this.controller, required this.isTitle})
      : super(key: key);

  @override
  _TextFieldCommentPengalamanState createState() => _TextFieldCommentPengalamanState();
}

class _TextFieldCommentPengalamanState extends State<TextFieldCommentPengalaman> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.maxWidth(context) / 1.1,
      height: (!widget.isTitle) ? CustomSize.maxHeight(context) / 4 : null,
      margin: const EdgeInsets.only(bottom: 16, top: 8),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Theme.of(context).bottomAppBarColor, width: 2)),
      child: TextField(
        maxLines: null,
        textInputAction: TextInputAction.done,
        controller: widget.controller,
        decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            hintText: widget.isTitle ? 'Write your name' : 'Write a comment...'),
      ),
    );
  }
}
