part of '_screens.dart';

class StoryBody extends StatelessWidget {
  final String title;
  final String sender;
  final String content;
  StoryBody({Key? key, required this.title, required this.sender, required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.maxWidth(context) * 0.9,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).hoverColor, fontSize: 25, fontWeight: FontWeight.w500),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 20),
            alignment: Alignment.centerLeft,
            child: Text(
              'by ' + sender,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              content,
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
