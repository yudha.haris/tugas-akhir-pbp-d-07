import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/obat/tambahkan_obat.dart';
import 'package:teman_covid/widgets/_widgets.dart';

class ListObat extends StatefulWidget {
  @override
  _ListObatState createState() => _ListObatState();
}

Future<List<dynamic>> fetchObat() async {
  final response =
      await http.get(Uri.parse('https://temancovid.herokuapp.com/informasi-obat/json'));

  if (response.statusCode == 200) {
    List<dynamic> list_of_obat = jsonDecode(response.body);
    return list_of_obat;
  } else {
    return [];
  }
}

class _ListObatState extends State<ListObat> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(children: [
            Container(
              height: 20,
            ),
            Container(
                alignment: Alignment.center,
                child: Text('Daftar Obat dan Suplemen',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30))),
            FutureBuilder<List<dynamic>>(
                future: fetchObat(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: snapshot.data!.length,
                        itemBuilder: (context, index) {
                          return Container(
                              padding: EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 8),
                              child: Column(children: [
                                Container(
                                    child: Text(
                                        (index + 1).toString() +
                                            '. ' +
                                            snapshot.data![index]['fields']['name'],
                                        style:
                                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
                                Container(
                                  child:
                                      Image.asset('assets/image/obat.jpg', height: 100, width: 300),
                                ),
                                Container(
                                    child: Text(snapshot.data![index]['fields']['info'],
                                        style: TextStyle(fontSize: 16))),
                              ]));
                        });
                  } else if (snapshot.hasError) {
                    return Container(
                      alignment: Alignment.center,
                      child: Text('Tidak ada data'),
                    );
                  }
                  return Container(child: const CircularProgressIndicator());
                }),
            Container(child: Consumer<LoginState>(builder: (context, loginState, _) {
              return ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                    primary: Colors.lightGreen,
                  ),
                  onPressed: () async {
                    if (loginState.username == 'rasasayange') {
                      Navigator.push(
                          context, CupertinoPageRoute(builder: (context) => TambahkanObat()));
                    } else {
                      showDialog<void>(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              title: Text('Whoops :(',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Theme.of(context).cardColor)),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text('Anda harus login sebagai admin terlebih dahulu',
                                        style:
                                            TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    // Go back to page before pop-up
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                              elevation: 24.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0)));
                        },
                      );
                    }
                  },
                  child:
                      const Text('Tambahkan', style: TextStyle(fontSize: 12, color: Colors.black)));
            })),
          ])),
    );
  }
}
