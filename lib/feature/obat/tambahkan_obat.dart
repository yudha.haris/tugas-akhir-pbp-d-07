import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/widgets/_widgets.dart';

class TambahkanObat extends StatefulWidget {
  @override
  _TambahkanObatState createState() => _TambahkanObatState();
}

class _TambahkanObatState extends State<TambahkanObat> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _infoController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(
        height: CustomSize.maxHeight(context),
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(children: <Widget>[
            SizedBox(height: 110),
            Container(
              alignment: Alignment.center,
              child: const Text(
                "Masukan Informasi Obat yang Ingin Ditambahkan",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 20),
            Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      child: Text("Nama Obat"),
                    ),
                    Container(
                      constraints: BoxConstraints(
                          minWidth: 100, maxWidth: 200, minHeight: 40, maxHeight: 40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(const Radius.circular(10)),
                        border: Border.all(color: Colors.blueGrey, width: 1),
                      ),
                      child: TextField(
                        textAlign: TextAlign.center,
                        controller: _nameController,
                        decoration: const InputDecoration(hintText: "Name"),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      child: Text("Info Obat"),
                    ),
                    Container(
                      constraints: BoxConstraints(
                          minWidth: 100, maxWidth: 200, minHeight: 100, maxHeight: 100),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(const Radius.circular(10)),
                          border: Border.all(color: Colors.blueGrey, width: 1)),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        textAlign: TextAlign.center,
                        controller: _infoController,
                        decoration: const InputDecoration(hintText: "Your Info Here"),
                      ),
                    ),
                    Container(
                        child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.green),
                      onPressed: () async {
                        if (_nameController.text.isNotEmpty && _infoController.text.isNotEmpty) {
                          final http.Response response = await http.post(
                              Uri.parse('https://temancovid.herokuapp.com/informasi-obat/post'),
                              body: {
                                "name": _nameController.text,
                                "info": _infoController.text,
                              });
                          setState(() {
                            _nameController.text = "";
                            _infoController.text = "";
                          });
                          Navigator.pop(context);
                        }
                        else{
                          showDialog<void>(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                  title: Text('Whoops :(',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Theme.of(context).cardColor)),
                                  content: SingleChildScrollView(
                                    child: ListBody(
                                      children: <Widget>[
                                        Text('Mohon Isi Semua Field',
                                            style:
                                            TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                      ],
                                    ),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      child: const Text('OK'),
                                      onPressed: () {
                                        // Go back to page before pop-up
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                  elevation: 24.0,
                                  shape: new RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(20.0)));
                            },
                          );
                        }
                      },
                      child: Text("Submit"),
                    )),
                  ],
                ))
          ]),
        ),
      ),
    );
  }
}
