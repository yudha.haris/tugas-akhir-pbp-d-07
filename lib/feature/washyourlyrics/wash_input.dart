import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/feature/washyourlyrics/wash_output.dart';
import 'package:teman_covid/widgets/_widgets.dart';

class WashInput extends StatefulWidget {
  @override
  _WashInputState createState() => _WashInputState();
}

class _WashInputState extends State<WashInput> {
  TextEditingController _baris1 = TextEditingController();
  TextEditingController _baris2 = TextEditingController();
  TextEditingController _baris3 = TextEditingController();
  TextEditingController _baris4 = TextEditingController();
  TextEditingController _baris5 = TextEditingController();
  TextEditingController _baris6 = TextEditingController();
  TextEditingController _baris7 = TextEditingController();
  TextEditingController _baris8 = TextEditingController();
  TextEditingController _baris9 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 0),
          child: Column(
            children: [
              Text(
                "Wash Your Lyrics",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0,
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                  margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                  child: Column(children: <Widget>[
                    Text(
                      "Tahukah kamu bahwa diperlukan waktu selama minimal 20 detik untuk mencuci tangan hingga bersih? Kalau begitu, mari mencuci tangan sambil menyanyikan lagu kesukaanmu!",
                      style: TextStyle(fontSize: 16.0, color: Theme.of(context).cardColor),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "(versi orisinal: washyourlyrics.com)",
                      style: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      textAlign: TextAlign.center,
                    ),
                  ])),
              Container(
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: Image.asset('assets/image/wash/illustration.png'),
              ),
              Text(
                'Baris 1',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                ),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris1,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 2',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris2,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 3',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris3,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 4',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris4,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 5',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris5,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 6',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris6,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 7',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris7,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 8',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris8,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Text(
                'Baris 9',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, color: Theme.of(context).cardColor),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor)),
                margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                child: TextField(
                  controller: _baris9,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Masukkan lirikmu....',
                      hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 24),
                child: ElevatedButton(
                  onPressed: () async {
                    if (_baris1.text != '' &&
                        _baris2.text != '' &&
                        _baris3.text != '' &&
                        _baris4.text != '' &&
                        _baris5.text != '' &&
                        _baris6.text != '' &&
                        _baris7.text != '' &&
                        _baris8.text != '' &&
                        _baris9.text != '') {
                      final http.Response response = await http.post(
                          Uri.parse('https://temancovid.herokuapp.com/wash-your-lyrics/post'),
                          body: {
                            "Baris1": _baris1.text,
                            "Baris2": _baris2.text,
                            "Baris3": _baris3.text,
                            "Baris4": _baris4.text,
                            "Baris5": _baris5.text,
                            "Baris6": _baris6.text,
                            "Baris7": _baris7.text,
                            "Baris8": _baris8.text,
                            "Baris9": _baris9.text,
                          });

                      // Go to output page
                      Navigator.push(
                          context, CupertinoPageRoute(builder: (context) => WashOutput()));
                    } else {
                      return showDialog<void>(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              title: Text('Whoops :(',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Theme.of(context).cardColor)),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text('Masih ada baris yang kosong. Lengkapi dulu lirikmu , ya!',
                                        style:
                                            TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    // Go back to page before pop-up
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                              elevation: 24.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0)));
                        },
                      );
                    }
                  },
                  child: const Text('Submit',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      )),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    primary: Theme.of(context).cardColor,
                    minimumSize: const Size(200, 50),
                    maximumSize: const Size(200, 50),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
