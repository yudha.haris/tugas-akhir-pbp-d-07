import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/widgets/_widgets.dart';

class WashOutput extends StatefulWidget {
  @override
  _WashOutputState createState() => _WashOutputState();
}

Future<List<dynamic>> fetchLirik() async {
  final response = await http
      .get(Uri.parse('https://temancovid.herokuapp.com/wash-your-lyrics/json'));

  if (response.statusCode == 200) {
    List<dynamic> list_of_lyric = jsonDecode(response.body);
    return list_of_lyric;
  } else {
    throw Exception('Gagal memuatnya :(');
  }
}

class _WashOutputState extends State<WashOutput> {
  late Future<List<dynamic>> futureLirik;

  @override
  void initState() {
    super.initState();
    futureLirik = fetchLirik();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(
        child: FutureBuilder<List<dynamic>>(
          future: futureLirik,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    return Container(
                        margin: EdgeInsets.only(
                            left: 24, right: 24, top: 24, bottom: 24),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/01.jpg'),
                                  Text("1 - " +
                                      snapshot.data![index]['fields']['Baris1']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/02.jpg'),
                                  Text("2 - " +
                                      snapshot.data![index]['fields']['Baris2']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/03.jpg'),
                                  Text("3 - " +
                                      snapshot.data![index]['fields']['Baris3']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/04.jpg'),
                                  Text("4 - " +
                                      snapshot.data![index]['fields']['Baris4']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/05.jpg'),
                                  Text("5 - " +
                                      snapshot.data![index]['fields']['Baris5']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/06.jpg'),
                                  Text("6 - " +
                                      snapshot.data![index]['fields']['Baris6']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/07.jpg'),
                                  Text("7 - " +
                                      snapshot.data![index]['fields']['Baris7']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/08.jpg'),
                                  Text("8 - " +
                                      snapshot.data![index]['fields']['Baris8']
                                          .toString()),
                                ])),
                            Container(
                                decoration: BoxDecoration(),
                                margin: EdgeInsets.only(
                                    left: 24, right: 24, top: 24, bottom: 0),
                                child: Column(children: <Widget>[
                                  Image.asset('assets/image/wash/09.jpg'),
                                  Text("9 - " +
                                      snapshot.data![index]['fields']['Baris9']
                                          .toString()),
                                ])),
                            Container(
                              height: 32,
                            )
                          ],
                        ));
                  });
            } else if (snapshot.hasError) {
              return Column(
                children: [
                  Text('${snapshot.error}'),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Buat Lagi',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          )),
                      style: ElevatedButton.styleFrom(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        primary: Theme.of(context).cardColor,
                        minimumSize: const Size(200, 50),
                        maximumSize: const Size(200, 50),
                      ))
                ],
              );
            }
            // By default, show a loading spinner
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
