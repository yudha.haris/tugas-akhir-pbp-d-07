import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/widgets/_widgets.dart';

import 'edit_profile.dart';

class Profile extends StatefulWidget {
  String username;
  Profile({required this.username});

  @override
  _ProfileState createState() => _ProfileState();
}

Future<Map<String, dynamic>> fetchProfile(String username) async {
  final response = await http.post(Uri.parse('https://temancovid.herokuapp.com/user-profile/json'),
      body: {'username': username});

  if (response.statusCode == 200) {
    Map<String, dynamic> profile_info = jsonDecode(response.body);
    return profile_info;
  } else {
    throw Exception('Failed to load album');
  }
}

class _ProfileState extends State<Profile> {
  late Future<Map<String, dynamic>> futureProfile;

  @override
  void initState() {
    super.initState();
    futureProfile = fetchProfile(widget.username);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: Container(
            width: CustomSize.maxWidth(context),
            padding: const EdgeInsets.all(20.0),
            child: ListView(children: [
              const Text(
                "Your Profile",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
                textAlign: TextAlign.center,
              ),
              FutureBuilder<Map<String, dynamic>>(
                future: futureProfile,
                builder: (context, snapshot) {
                  // print(snapshot);
                  if (snapshot.hasData) {
                    return Container(
                        width: CustomSize.maxWidth(context),
                        margin: const EdgeInsets.only(top: 15),
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        decoration: BoxDecoration(
                          color: Theme.of(context).cardColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  child: Icon(Icons.account_circle, size: 150, color: Colors.white),
                                ),
                                Container(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text(
                                        snapshot.data!['user'].toString(),
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 25,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    Container(
                                        width: CustomSize.maxWidth(context) * 0.4,
                                        child: Text(
                                          snapshot.data!['bio'].toString(),
                                          style: const TextStyle(
                                            fontSize: 15,
                                            color: Colors.white54,
                                          ),
                                          textAlign: TextAlign.justify,
                                        ))
                                  ],
                                ))
                              ],
                            ),
                            Container(
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.lightGreen,
                                    ),
                                    child: const Text('Edit Profile'),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          CupertinoPageRoute(
                                              builder: (context) => EditProfile(
                                                    username: widget.username,
                                                    data: snapshot.data!,
                                                  )));
                                    })),
                            Container(
                                width: CustomSize.maxWidth(context),
                                margin: const EdgeInsets.only(top: 15),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                decoration: const BoxDecoration(
                                    color: Colors.lightGreen,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0),
                                    )),
                                child: const Text(
                                  "Personal Info",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                )),
                            Container(
                                width: CustomSize.maxWidth(context),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      child: Icon(Icons.person, size: 27, color: Colors.black),
                                    ),
                                    Expanded(
                                      child: Container(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            snapshot.data!['first_name'].toString() +
                                                " " +
                                                snapshot.data!['last_name'].toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                            ),
                                          )),
                                    ),
                                  ],
                                )),
                            Container(
                                width: CustomSize.maxWidth(context),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      child: Icon(Icons.email, size: 27, color: Colors.black),
                                    ),
                                    Expanded(
                                      child: Container(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            snapshot.data!['email'].toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                            ),
                                          )),
                                    ),
                                  ],
                                )),
                            Container(
                                width: CustomSize.maxWidth(context),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      child:
                                          Icon(Icons.emoji_emotions, size: 27, color: Colors.black),
                                    ),
                                    Expanded(
                                      child: Container(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            snapshot.data!['hobi'].toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                            ),
                                          )),
                                    ),
                                  ],
                                )),
                            Container(
                                width: CustomSize.maxWidth(context),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      child: Icon(Icons.camera_alt_outlined,
                                          size: 27, color: Colors.black),
                                    ),
                                    Expanded(
                                      child: Container(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            "@" + snapshot.data!['insta'].toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                            ),
                                          )),
                                    ),
                                  ],
                                )),
                            Container(
                                width: CustomSize.maxWidth(context),
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0),
                                      bottomRight: Radius.circular(10.0),
                                    )),
                                child: Row(
                                  children: [
                                    Container(
                                      child:
                                          Icon(Icons.house_rounded, size: 27, color: Colors.black),
                                    ),
                                    Expanded(
                                      child: Container(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            snapshot.data!['alamat'].toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                            ),
                                          )),
                                    ),
                                  ],
                                )),
                          ],
                        ));
                  } else if (snapshot.hasError) {
                    return Column(
                      children: [Text('${snapshot.error}')],
                    );
                  }
                  // By default, show a loading spinner.
                  return const CircularProgressIndicator();
                },
              ),
            ])));
  }
}
