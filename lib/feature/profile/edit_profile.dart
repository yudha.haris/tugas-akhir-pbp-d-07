import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/feature/profile/profile.dart';
import 'package:teman_covid/widgets/_widgets.dart';

class EditProfile extends StatefulWidget {
  String username;
  Map<String, dynamic> data;
  EditProfile({Key? key, required this.username, required this.data}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

Future<Map<String, dynamic>> fetchProfile(String username) async {
  final response = await http.post(Uri.parse('https://temancovid.herokuapp.com/user-profile/json'),
      body: {'username': username});

  if (response.statusCode == 200) {
    Map<String, dynamic> profile_info = jsonDecode(response.body);
    return profile_info;
  } else {
    throw Exception('Failed to load album');
  }
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController _username = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _first_name = TextEditingController();
  TextEditingController _last_name = TextEditingController();
  TextEditingController _bio = TextEditingController();
  TextEditingController _hobi = TextEditingController();
  TextEditingController _insta = TextEditingController();
  TextEditingController _alamat = TextEditingController();

  @override
  void initState() {
    super.initState();
    _username.text = widget.data['user'].toString();
    _email.text = widget.data['email'].toString();
    _first_name.text = widget.data['first_name'].toString();
    _last_name.text = widget.data['last_name'].toString();
    _bio.text = widget.data['bio'].toString();
    _hobi.text = widget.data['hobi'].toString();
    _insta.text = widget.data['insta'].toString();
    _alamat.text = widget.data['alamat'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: Container(
          width: CustomSize.maxWidth(context),
          padding: const EdgeInsets.all(20.0),
          child: ListView(children: [
            const Text(
              "Edit Profile",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
                width: CustomSize.maxWidth(context),
                margin: const EdgeInsets.only(top: 15),
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                decoration: BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      '       Username',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _username,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. johndoe3',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    const Text(""),
                    const Text(
                      '       Email',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: const TextStyle(color: Colors.white),
                        controller: _email,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. johndoe3@mail.com',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       First Name',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _first_name,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. John',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       Last Name',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _last_name,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. Doe',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       Bio',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _bio,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. hello nice to meet ya',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       Hobi',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _hobi,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. Likes to code',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       Instagram',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _insta,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. johndoe3',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                    Text(""),
                    const Text(
                      '       Alamat',
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
                      ),
                      margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        controller: _alamat,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'ex. District 6 Western Cloudream',
                            hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 24)),
                      ),
                    ),
                  ],
                )),
            Container(
                margin: const EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 24),
                child: Column(children: [
                  ElevatedButton(
                    onPressed: () async {
                      final http.Response response = await http.post(
                          Uri.parse('https://temancovid.herokuapp.com/user-profile/post'),
                          body: {
                            "username": _username.text,
                            "email": _email.text,
                            "first_name": _first_name.text,
                            "last_name": _last_name.text,
                            "bio": _bio.text,
                            "hobi": _hobi.text,
                            "insta": _insta.text,
                            "alamat": _alamat.text,
                          });
                      if (response.statusCode == 200) {
                        // Kalau sukses ngapain serah
                        // ...
                        // ...
                        print("SUKSES");
                      } else {
                        // Kalau gagal ngapain serah
                        // ...
                        // ...
                      }

                      // Abis berhasil ngirim, dia otomatis pindah halaman
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => Profile(
                                    username: widget.username,
                                  )));
                    },
                    child: const Text('Submit',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        )),
                    style: ElevatedButton.styleFrom(
                      shape:
                          new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      primary: Colors.lightGreen,
                      minimumSize: const Size(200, 50),
                      maximumSize: const Size(200, 50),
                    ),
                  ),
                ]))
          ]),
        ));
  }
}
