import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:teman_covid/feature/sharepengalaman/stories.dart';
import '../../widgets/_widgets.dart';
import 'package:http/http.dart' as http;

class AddStories extends StatefulWidget {
  @override
  _AddStoriesState createState() => _AddStoriesState();
}

class _AddStoriesState extends State<AddStories> {
  final _formKey = GlobalKey<FormState>();
  bool _isLogin = true;

  String value = "";
  @override
  Widget build(BuildContext context) {
    final _nameController = TextEditingController();
    final _titleController = TextEditingController();
    final _stroyController = TextEditingController();
    final _captchaController = TextEditingController();
    const primaryColor = Color(0xFF8DB580);
    FocusNode myFocusNode = new FocusNode();
    FocusNode myFocusNode2 = new FocusNode();
    FocusNode myFocusNode3 = new FocusNode();
    FocusNode myFocusNode4 = new FocusNode();
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Center(
          child: Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Text(
                      "SHARE YOUR STORY",
                      style: TextStyle(
                          fontSize: 40.0, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF4C6245),
                        hintText: "Write the Title of Your Story",
                        labelText: "Title",
                        icon: Icon(Icons.title_outlined),
                        labelStyle: TextStyle(
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      controller: _titleController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Title can\'t be empty';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF4C6245),
                        hintText: 'Can be anonymous',
                        labelText: "By",
                        icon: Icon(Icons.people),
                        labelStyle: TextStyle(
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      controller: _nameController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Fill your identity';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      maxLines: null,
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF4C6245),
                        hintText: 'Story',
                        labelText: "Your Story",
                        labelStyle: TextStyle(
                          color: Colors.white,
                        ),
                        icon: Icon(Icons.explore),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 25.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      controller: _stroyController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Write your story';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                    child: TextFormField(
                      focusNode: myFocusNode,
                      style: TextStyle(color: Colors.white),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF4C6245),
                        hintText: 'Proove that you are a human',
                        labelText: "1+1 =",
                        labelStyle: TextStyle(
                          color: Colors.white,
                        ),
                        icon: Icon(Icons.question_answer),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      controller: _captchaController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Answer the question';
                        }
                        if (value != '2') {
                          return 'Wrong answer';
                        }
                        return null;
                      },
                    ),
                  ),
                  ElevatedButton(
                    child: Text(
                      "Share",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        Color(0xFF4C6245),
                      ),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        // print("Thanks for sharing your story, " +
                        //     _nameController.text +
                        //     "!");
                        final http.Response response = await http.post(
                            Uri.parse(
                                'https://temancovid.herokuapp.com/story/post'),
                            body: {
                              "title": _titleController.text,
                              "by": _nameController.text,
                              "experience": _stroyController.text,
                            });
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => Stories()));
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      )),
    );
  }
}
