import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/commentpengalaman/_screens.dart';
import 'package:teman_covid/feature/sharepengalaman/add_stories.dart';
import 'package:teman_covid/widgets/_widgets.dart';

import '../../widgets/_widgets.dart';

class Stories extends StatefulWidget {
  @override
  _StoriesState createState() => _StoriesState();
}

Future<List<dynamic>> fetchStories() async {
  final response = await http.get(
    Uri.parse('https://temancovid.herokuapp.com/story/json'),
    // headers: {
    //   "Access-Control-Allow-Origin": "*", // Required for CORS support to work
    // "Access-Control-Allow-Credentials":
    //     "true", // Required for cookies, authorization headers with HTTPS
    // "Access-Control-Allow-Headers":
    //     "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
    // "Access-Control-Allow-Methods": "GET, POST"
    // },
  );

  if (response.statusCode == 200) {
    List<dynamic> stories = jsonDecode(response.body);
    return stories;
  } else {
    throw Exception('Failed to load stories');
  }
}

class _StoriesState extends State<Stories> {
  late Future<List<dynamic>> futureStories;
  @override
  void initState() {
    super.initState();
    futureStories = fetchStories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width / 1.1,
            height: CustomSize.maxHeight(context),
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        padding: const EdgeInsets.only(left: 30, top: 5.0),
                        child: const Center(
                          child: Text(
                            "OUR STORIES",
                            style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
                          ),
                        )),
                    Center(
                      child: Consumer<LoginState>(builder: (context, loginState, _) {
                        return ElevatedButton(
                            onPressed: () {
                              if (loginState.isLogin) {
                                Navigator.push(context,
                                    CupertinoPageRoute(builder: (context) => AddStories()));
                              } else {
                                showDialog<void>(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                        title: Text('Whoops :(',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                color: Theme.of(context).cardColor)),
                                        content: SingleChildScrollView(
                                          child: ListBody(
                                            children: <Widget>[
                                              Text('Anda harus login terlebih dahulu',
                                                  style: TextStyle(
                                                      color: Theme.of(context).bottomAppBarColor)),
                                            ],
                                          ),
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            child: const Text('OK'),
                                            onPressed: () {
                                              // Go back to page before pop-up
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                        elevation: 24.0,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(20.0)));
                                  },
                                );
                              }
                            },
                            child: const Text(
                              'Share Yours',
                              style: TextStyle(fontSize: 10),
                            ),
                            style: ElevatedButton.styleFrom(
                              shape:
                                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                              primary: Theme.of(context).cardColor,
                              minimumSize: const Size(100, 35),
                              maximumSize: const Size(100, 35),
                            ));
                      }),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 20),
                      padding: const EdgeInsets.only(bottom: 10),
                      child: FutureBuilder<List<dynamic>>(
                        future: futureStories,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                // gridDelegate:
                                //     const SliverGridDelegateWithFixedCrossAxisCount(
                                //   crossAxisCount: 2,
                                //   crossAxisSpacing: 5.0,
                                //   mainAxisSpacing: 5.0,
                                // ),
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(top: 20.0),
                                    padding: EdgeInsets.only(left: 25, right: 25, top: 10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          snapshot.data![index]['fields']['title'],
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold, fontSize: 20.0),
                                        ),
                                        Text(
                                          "by: ${snapshot.data![index]['fields']['by']}",
                                          style: const TextStyle(fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          snapshot.data![index]['fields']['experience'],
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        ElevatedButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (context) => CommentPengalaman(
                                                            postTitle: snapshot.data![index]
                                                                ['fields']['title'],
                                                            postBody: snapshot.data![index]
                                                                ['fields']['experience'],
                                                            postSender: snapshot.data![index]
                                                                ['fields']['by'],
                                                            slug: snapshot.data![index]['fields']
                                                                ['slug'],
                                                          )));
                                            },
                                            child: const Text(
                                              'Read More',
                                              style: TextStyle(fontSize: 10),
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(30.0)),
                                              primary: Theme.of(context).cardColor,
                                              minimumSize: const Size(100, 35),
                                              maximumSize: const Size(100, 35),
                                            ))
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        color: Color(0xFF8DB580),
                                        borderRadius: BorderRadius.circular(15)),
                                  );
                                });
                          } else if (snapshot.hasError) {
                            return Column(
                              children: [
                                Text('${snapshot.error}'),
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text('Kembali',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                        )),
                                    style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30.0)),
                                      primary: Theme.of(context).cardColor,
                                      minimumSize: const Size(200, 50),
                                      maximumSize: const Size(200, 50),
                                    )),
                              ],
                            );
                          }
                          // By default, show a loading spinner
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ]),
            ),
          ),
        ));
  }
}
