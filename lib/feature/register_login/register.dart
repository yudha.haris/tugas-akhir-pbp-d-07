import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teman_covid/widgets/_widgets.dart';

import 'login.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController password1Controller = TextEditingController();
  TextEditingController password2Controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: SingleChildScrollView(
            child: Container(
          margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 0),
          child: Column(children: [
            const SizedBox(
              //Use of SizedBox
              height: 15,
            ),
            const Text('Get Started.',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.black87)),
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: Image.asset('assets/image/register/regist_illust.png'),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Username*',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Email*',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                obscureText: true,
                controller: password1Controller,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Password*',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: const <Widget>[
                Text('• Your password can’t be too similar to your other personal information.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 12, color: Colors.blueGrey)),
                Text('• Your password must contain at least 8 characters.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 12, color: Colors.blueGrey)),
                Text('• Your password can’t be a commonly used password.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 12, color: Colors.blueGrey)),
                Text('• Your password can’t be entirely numeric.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 12, color: Colors.blueGrey)),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                obscureText: true,
                controller: password2Controller,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Password Confirmation*',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            const Text('Enter the same password as before, for verification.',
                textAlign: TextAlign.start,
                style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 12, color: Colors.blueGrey)),
            SizedBox(
              //Use of SizedBox
              height: 15,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child: ElevatedButton(
                onPressed: () async {
                  if (nameController.text != '' &&
                      emailController.text != '' &&
                      password1Controller.text != '' &&
                      password2Controller.text != '' &&
                      password1Controller.text == password2Controller.text) {
                    final http.Response response = await http
                        .post(Uri.parse("https://temancovid.herokuapp.com/register/post"), body: {
                      "username": nameController.text,
                      "email": emailController.text,
                      "password1": password1Controller.text
                    });
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => Login()));
                  } else if (password1Controller.text != password2Controller.text) {
                    showDialog<void>(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                            title: Text('Whoops :(',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Theme.of(context).cardColor)),
                            content: SingleChildScrollView(
                              child: ListBody(
                                children: <Widget>[
                                  Text('Konfirmasi password salah',
                                      style: TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                ],
                              ),
                            ),
                            actions: <Widget>[
                              TextButton(
                                child: const Text('OK'),
                                onPressed: () {
                                  // Go back to page before pop-up
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                            elevation: 24.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0)));
                      },
                    );
                  } else {
                    showDialog<void>(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                            title: Text('Whoops :(',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Theme.of(context).cardColor)),
                            content: SingleChildScrollView(
                              child: ListBody(
                                children: <Widget>[
                                  Text('Semua field harus dipenuhi',
                                      style: TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                ],
                              ),
                            ),
                            actions: <Widget>[
                              TextButton(
                                child: const Text('OK'),
                                onPressed: () {
                                  // Go back to page before pop-up
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                            elevation: 24.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0)));
                      },
                    );
                  }
                },
                child: Container(
                    alignment: Alignment.center,
                    width: 100,
                    child: Text(
                      'Daftar',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(20.0),
                    primary: Theme.of(context).cardColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))),
              ),
            ),
            const SizedBox(
              //Use of SizedBox
              height: 100,
            ),
            const Text('Copyright © 2021 PBP D07 University of Indonesia',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15, color: Colors.black87)),
            const SizedBox(
              //Use of SizedBox
              height: 15,
            ),
          ]),
        )));
  }
}
