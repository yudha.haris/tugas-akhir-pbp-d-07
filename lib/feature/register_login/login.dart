import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:teman_covid/core/constants/custom_size.dart';
import 'package:teman_covid/core/states/login_state.dart';
import 'package:teman_covid/feature/homepage/homepage.dart';
import 'package:teman_covid/widgets/_widgets.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: MediaQuery.of(context).size / 12,
          child: CustomAppBar(),
        ),
        drawer: MainDrawer(),
        body: SingleChildScrollView(
            child: Container(
          margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 0),
          child: Column(children: [
            SizedBox(
              //Use of SizedBox
              height: 15,
            ),
            const Text('Welcome Back!',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.black87)),
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: Image.asset('assets/image/register/login_illust.png'),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Username',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Theme.of(context).bottomAppBarColor),
              ),
              margin: EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Password',
                    hintStyle: TextStyle(color: Theme.of(context).bottomAppBarColor),
                    contentPadding: EdgeInsets.symmetric(horizontal: 24)),
              ),
            ),
            SizedBox(
              //Use of SizedBox
              height: 15,
            ),
            Consumer<LoginState>(builder: (context, loginState, _) {
              return Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: ElevatedButton(
                  onPressed: () async {
                    final http.Response response = await http
                        .post(Uri.parse("https://temancovid.herokuapp.com/auth/login"), body: {
                      "username": nameController.text,
                      "password": passwordController.text,
                    });
                    print(response.statusCode);
                    if (response.statusCode == 200) {
                      print(response.body);
                      Map<String, dynamic> stories = jsonDecode(response.body);
                      if (stories['login']) {
                        String usernames = stories['username'];
                        loginState.setUsername(usernames);
                        loginState.setLogin(true);
                        Navigator.pushReplacement(
                            context, CupertinoPageRoute(builder: (context) => HomePage()));
                      } else {
                        showDialog<void>(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return AlertDialog(
                                title: Text('Whoops :(',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Theme.of(context).cardColor)),
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                      Text('Login gagal',
                                          style: TextStyle(
                                              color: Theme.of(context).bottomAppBarColor)),
                                    ],
                                  ),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    child: const Text('OK'),
                                    onPressed: () {
                                      // Go back to page before pop-up
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                                elevation: 24.0,
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(20.0)));
                          },
                        );
                      }
                    } else {
                      showDialog<void>(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              title: Text('Whoops :(',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Theme.of(context).cardColor)),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text('Login gagal',
                                        style:
                                            TextStyle(color: Theme.of(context).bottomAppBarColor)),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    // Go back to page before pop-up
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                              elevation: 24.0,
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0)));
                        },
                      );
                    }
                  },
                  child: Container(
                      alignment: Alignment.center,
                      width: 100,
                      child: const Text(
                        'Masuk',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(20.0),
                      primary: Theme.of(context).cardColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))),
                ),
              );
            }),
            const SizedBox(
              //Use of SizedBox
              height: 100,
            ),
            Container(
              padding: EdgeInsets.only(bottom: 16, top: 16),
              width: CustomSize.maxWidth(context),
              alignment: Alignment.center,
              color: Theme.of(context).bottomAppBarColor,
              child: const Text('Copyright © 2021 PBP D07 University of Indonesia',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 15, color: Colors.black87)),
            ),
          ]),
        )));
  }
}
